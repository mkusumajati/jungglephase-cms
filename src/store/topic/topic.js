import fetch from '../../fetch'
export default {
  namespaced: true,
  state: {
    listTopic: null,
  },
  mutations: {
    SET: (state, payload) => {
      state.listTopic = payload
    },
    ADD (state, data) {
      state.listTopic.push(data)
    },
    DELETE (state, id) {
      const index = state.listTopic.findIndex(item => item.id === id)
      state.listTopic.splice(index, 1)
    },
    UPDATE (state, data) {
      state.listTopic = state.listTopic.map(topic => {
        if (topic.id === data.id) {
          return Object.assign({}, topic, data)
        }
        return topic
      })
    },
  },
  actions: {
    async ACTION_GET ({ commit }) {
      await fetch().get('/topics').then(res => {
        commit('SET', res.data.data.result)
      })
    },
    ACTION_ADD ({ commit }, data) {
      return new Promise((resolve, reject) => {
        fetch().post('/topics', data).then(res => {
        commit('ADD', res.data.data.result)
        resolve(true)
      })
      })
    },
    ACTION_DELETE ({ commit }, data) {
      return new Promise((resolve, reject) => {
        fetch().delete(`/topics/${data}`).then(() => {
          commit('DELETE', data)
          resolve()
        })
      })
    },
    ACTION_UPDATE ({ commit }, data) {
      return new Promise((resolve, reject) => {
        fetch().put(`/topics/${data.id}`, data).then(res => {
          commit('UPDATE', res.data.data.result)
          resolve()
        })
      })
    },
  },
  getters: {
    GET_LIST_TOPIC (state) {
      return state.listTopic
    },
  },
}
