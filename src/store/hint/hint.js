import fetch from '../../fetch'
export default {
  namespaced: true,
  state: {
    listHint: null,
  },
  mutations: {
    SET: (state, payload) => {
      state.listHint = payload
    },
    ADD (state, data) {
      state.listHint.push(data)
    },
    DELETE (state, id) {
      const index = state.listHint.findIndex(item => item.id === id)
      state.listHint.splice(index, 1)
    },
    UPDATE (state, data) {
      const param = {
        name: data.name,
        label: data.label,
        description: data.description,
      }
      state.listHint = state.listHint.map(hint => {
        if (hint.id === data.id) {
          return Object.assign({}, hint, param)
        }
        return hint
      })
    },
  },
  actions: {
    async ACTION_GET ({ commit }) {
      await fetch().get('/hints').then(res => {
        commit('SET', res.data.data.result)
      })
    },
    ACTION_ADD ({ commit }, data) {
      return new Promise((resolve, reject) => {
        fetch().post('/hints', data).then(res => {
        commit('ADD', res.data.data.result)
        resolve(true)
      })
      })
    },
    ACTION_DELETE ({ commit }, data) {
      return new Promise((resolve, reject) => {
        fetch().delete(`/hints/${data}`).then(() => {
          commit('DELETE', data)
          resolve()
        })
      })
    },
    ACTION_UPDATE ({ commit }, data) {
      const param = {
        name: data.name,
        label: data.label,
        description: data.description,
      }
      return new Promise((resolve, reject) => {
        fetch().put(`/hints/${data.id}`, param).then(res => {
          commit('UPDATE', res.data.data.result)
          resolve()
        })
      })
    },
  },
  getters: {
    GET_LIST_HINT (state) {
      return state.listHint
    },
  },
}
