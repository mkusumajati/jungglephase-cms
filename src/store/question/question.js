import fetch from '../../fetch'
export default {
  namespaced: true,
  state: {
    listQuestion: null,
  },
  mutations: {
    SET: (state, payload) => {
      state.listQuestion = payload
    },
    ADD (state, data) {
      state.listQuestion.push(data)
    },
    DELETE (state, id) {
      const index = state.listQuestion.findIndex(item => item.id === id)
      state.listQuestion.splice(index, 1)
    },
    UPDATE (state, data) {
      state.listQuestion = state.listQuestion.map(question => {
        if (question.id === data.id) {
          return Object.assign({}, question, data)
        }
        return question
      })
    },
  },
  actions: {
    async ACTION_GET ({ commit }) {
      await fetch().get('/questions').then(res => {
        commit('SET', res.data.data.result)
      })
    },
    ACTION_ADD ({ commit }, data) {
      return new Promise((resolve, reject) => {
        fetch().post('/questions', data).then(res => {
        commit('ADD', res.data.data.result)
        resolve(true)
      })
      })
    },
    ACTION_DELETE ({ commit }, data) {
      return new Promise((resolve, reject) => {
        fetch().delete(`/questions/${data}`).then(() => {
          commit('DELETE', data)
          resolve()
        })
      })
    },
    ACTION_UPDATE ({ commit }, data) {
        return new Promise((resolve, reject) => {
        fetch().put(`/questions/${data.id}`, data).then(res => {
        commit('UPDATE', res.data.data.result)
          resolve()
        })
      })
    },
  },
  getters: {
    GET_LIST_QUESTION (state) {
      return state.listQuestion
    },
  },
}
