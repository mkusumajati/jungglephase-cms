import fetch from '../../fetch'
export default {
  namespaced: true,
  state: {
    listUsers: null,
  },
  mutations: {
    SET_TODO: (state, payload) => {
      state.listUsers = payload
    },
    ADD (state, data) {
      state.listUsers.push(data)
    },
    DELETE (state, id) {
      const index = state.listUsers.findIndex(item => item.id === id)
      state.listUsers.splice(index, 1)
    },
    UPDATE (state, data) {
      const param = {
        nickname: data.nickname,
        email: data.email,
        role: data.role,
      }
      state.listUsers = state.listUsers.map(users => {
        if (users.id === data.id) {
          return Object.assign({}, users, param)
        }
        return users
      })
    },
  },
  actions: {
    async GET_TODO ({ commit }) {
      await fetch().get('/users').then(res => {
        commit('SET_TODO', res.data.data.result)
      })
    },
    ACTION_ADD ({ commit }, data) {
      return new Promise((resolve, reject) => {
        fetch().post('/users', data).then(res => {
        commit('ADD', res.data.data.result)
        resolve(true)
      })
      })
    },
    ACTION_DELETE ({ commit }, data) {
      return new Promise((resolve, reject) => {
        fetch().delete(`/users/${data}`).then(() => {
          commit('DELETE', data)
          resolve()
        })
      })
    },
    ACTION_UPDATE ({ commit }, data) {
      const param = {
        nickname: data.nickname,
        email: data.email,
        role_id: data.role_id,
      }
      return new Promise((resolve, reject) => {
        fetch().put(`/users/${data.id}`, param).then(res => {
          commit('UPDATE', res.data.data.result)
          resolve()
        })
      })
    },
  },
  getters: {
    GET_LIST_USERS (state) {
      return state.listUsers
    },
  },
}
