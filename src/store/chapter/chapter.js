import fetch from '../../fetch'
export default {
  namespaced: true,
  state: {
    listChapter: null,
  },
  mutations: {
    SET: (state, payload) => {
      state.listChapter = payload
    },
    ADD (state, data) {
      state.listChapter.push(data)
    },
    DELETE (state, id) {
      const index = state.listChapter.findIndex(item => item.id === id)
      state.listChapter.splice(index, 1)
    },
    UPDATE (state, data) {
      state.listChapter = state.listChapter.map(chapter => {
        if (chapter.id === data.id) {
          return Object.assign({}, chapter, data)
        }
        return chapter
      })
    },
  },
  actions: {
    async ACTION_GET ({ commit }) {
      await fetch().get('/chapters').then(res => {
        commit('SET', res.data.data.result)
      })
    },
    ACTION_ADD ({ commit }, data) {
      return new Promise((resolve, reject) => {
        fetch().post('/chapters', data).then(res => {
          commit('ADD', res.data.data.result)
          resolve(true)
        })
      })
    },
    ACTION_DELETE ({ commit }, data) {
      return new Promise((resolve, reject) => {
        fetch().delete(`/chapters/${data}`).then(() => {
          commit('DELETE', data)
          resolve()
        })
      })
    },
    ACTION_UPDATE ({ commit }, data) {
      return new Promise((resolve, reject) => {
        fetch().put(`/chapters/${data.id}`, data).then(res => {
          commit('UPDATE', res.data.data.result)
          resolve()
        })
      })
    },
  },
  getters: {
    GET_LIST_CHAPTER (state) {
      return state.listChapter
    },
  },
}
