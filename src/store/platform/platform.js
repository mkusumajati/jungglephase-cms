import fetch from '../../fetch'
export default {
  namespaced: true,
  state: {
    listPlatform: null,
  },
  mutations: {
    SET: (state, payload) => {
      state.listPlatform = payload
    },
    ADD (state, data) {
      state.listPlatform.push(data)
    },
    DELETE (state, id) {
      const index = state.listPlatform.findIndex(item => item.id === id)
      state.listPlatform.splice(index, 1)
    },
    UPDATE (state, data) {
      const param = {
        name: data.name,
        label: data.label,
        description: data.description,
      }
      state.listPlatform = state.listPlatform.map(platform => {
        if (platform.id === data.id) {
          return Object.assign({}, platform, param)
        }
        return platform
      })
    },
  },
  actions: {
    async ACTION_GET ({ commit }) {
      await fetch().get('/platforms').then(res => {
        commit('SET', res.data.data.result)
      })
    },
    ACTION_ADD ({ commit }, data) {
      return new Promise((resolve, reject) => {
        fetch().post('/platforms', data).then(res => {
        commit('ADD', res.data.data.result)
        resolve(true)
      })
      })
    },
    ACTION_DELETE ({ commit }, data) {
      return new Promise((resolve, reject) => {
        fetch().delete(`/platforms/${data}`).then(() => {
          commit('DELETE', data)
          resolve()
        })
      })
    },
    ACTION_UPDATE ({ commit }, data) {
      const param = {
        name: data.name,
        label: data.label,
        description: data.description,
      }
      return new Promise((resolve, reject) => {
        fetch().put(`/platforms/${data.id}`, param).then(res => {
          commit('UPDATE', res.data.data.result)
          resolve()
        })
      })
    },
  },
  getters: {
    GET_LIST_PLATFORM (state) {
      return state.listPlatform
    },
  },
}
