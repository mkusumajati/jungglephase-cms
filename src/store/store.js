import Vue from 'vue'
import Vuex from 'vuex'

import creative from './creative/creative'
import login from './login/login'
import users from './users/users'
import platform from './platform/platform'
import courses from './course/course'
import chapters from './chapter/chapter'
import topics from './topic/topic'
import questions from './question/question'
import hints from './hint/hint'
import levels from './level/level'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    barColor: 'rgba(0, 0, 0, .8), rgba(0, 0, 0, .8)',
    barImage: 'https://staging-binar-academy.s3.ap-southeast-1.amazonaws.com/strapi/36473ee165e24186b95d93aaf22acc1e.jpg',
    drawer: null,
  },
  mutations: {
    SET_BAR_IMAGE (state, payload) {
      state.barImage = payload
    },
    SET_DRAWER (state, payload) {
      state.drawer = payload
    },
    SET_SCRIM (state, payload) {
      state.barColor = payload
    },
  },
  modules: {
    creative,
    login,
    users,
    platform,
    courses,
    chapters,
    topics,
    questions,
    hints,
    levels,
  },
})
