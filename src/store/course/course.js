import fetch from '../../fetch'
export default {
  namespaced: true,
  state: {
    listCourse: null,
  },
  mutations: {
    SET: (state, payload) => {
      state.listCourse = payload
    },
    ADD (state, data) {
      state.listCourse.push(data)
    },
    DELETE (state, id) {
      const index = state.listCourse.findIndex(item => item.id === id)
      state.listCourse.splice(index, 1)
    },
    UPDATE (state, data) {
      state.listCourse = state.listCourse.map(item => {
        if (item.id === data.id) {
          return Object.assign({}, item, data)
        }
        return item
      })
    },
  },
  actions: {
    async ACTION_GET ({ commit }) {
      await fetch().get('/courses').then(res => {
        commit('SET', res.data.data.result)
      })
    },
    ACTION_ADD ({ commit }, data) {
      return new Promise((resolve, reject) => {
        fetch().post('/courses', data).then(res => {
          commit('ADD', res.data.data.result)
          resolve(true)
        })
      })
    },
    ACTION_DELETE ({ commit }, data) {
      return new Promise((resolve, reject) => {
        fetch().delete(`/courses/${data}`).then(() => {
          commit('DELETE', data)
          resolve()
        })
      })
    },
    ACTION_UPDATE ({ commit }, data) {
      return new Promise((resolve, reject) => {
        fetch().put(`/courses/${data.id}`, data).then(res => {
          commit('UPDATE', res.data.data.result)
          resolve()
        })
      })
    },
  },
  getters: {
    GET_LIST_COURSE (state) {
      return state.listCourse
    },
  },
}
