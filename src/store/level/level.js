import fetch from '../../fetch'
export default {
  namespaced: true,
  state: {
    listLevel: null,
  },
  mutations: {
    SET: (state, payload) => {
      state.listLevel = payload
    },
  },
  actions: {
    async ACTION_GET ({ commit }) {
      await fetch().get('/levels').then(res => {
        commit('SET', res.data.data.result)
      })
    },
  },
  getters: {
    GET_LIST_LEVEL (state) {
      return state.listLevel
    },
  },
}
