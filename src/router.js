import Vue from 'vue'
import Router from 'vue-router'
import store from './store/store'

Vue.use(Router)

let router = new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: () => import('@/views/pages/Index'),
      children: [
        {
          name: 'Login',
          path: '',
          component: () => import('@/views/pages/Login'),
        },
      ],
    },
    {
      path: '/',
      component: () => import('@/views/dashboard/Index'),
      children: [
        // Dashboard
        {
          name: 'Dashboard',
          path: 'dashboard',
          component: () => import('@/views/dashboard/Dashboard'),
          meta: {
            requiresAuth: true,
          },
        },
        // Pages
        {
          name: 'User',
          path: 'user',
          component: () => import('@/views/dashboard/user/DataTables'),
          meta: {
            requiresAuth: true,
            permissions: [
              {
                role: store.getters.authRole === 'admin',
                access: true,
              },
              {
                role: store.getters.authRole !== 'admin',
                access: false,
                redirect: '/',
              },
            ],
          },
        },
        {
          name: 'Profile',
          path: 'profile',
          component: () => import('@/views/dashboard/user/UserProfile'),
          meta: {
            requiresAuth: true,
          },
        },
        {
          name: 'Platform',
          path: 'platform',
          component: () => import('@/views/dashboard/platform/DataTables'),
          meta: {
            requiresAuth: true,
          },
        },
        {
          name: 'Course',
          path: 'course-material',
          component: () => import('@/views/dashboard/course/DataTables'),
          meta: {
            requiresAuth: true,
          },
        },
        {
          name: 'Topic',
          path: 'topic',
          component: () => import('@/views/dashboard/topic/DataTables'),
          meta: {
            requiresAuth: true,
          },
        },
        {
          name: 'Chapter',
          path: 'chapter',
          component: () => import('@/views/dashboard/chapter/DataTables'),
          meta: {
            requiresAuth: true,
          },
        },
        {
          name: 'Question',
          path: 'question',
          component: () => import('@/views/dashboard/question/DataTables'),
          meta: {
            requiresAuth: true,
          },
        },
        {
          name: 'Hint',
          path: 'hint',
          component: () => import('@/views/dashboard/hint/DataTables'),
          meta: {
            requiresAuth: true,
          },
        },
        {
          name: 'News',
          path: 'binar-news',
          component: () => import('@/views/dashboard/news/DataTables'),
          meta: {
            requiresAuth: true,
          },
        },
        {
          name: 'Library',
          path: 'library',
          component: () => import('@/views/dashboard/library/DataTables'),
          meta: {
            requiresAuth: true,
          },
        },
      ],
    },
    {
      path: '*',
      component: () => import('@/views/pages/Index'),
      children: [
        {
          name: '404 Error',
          path: '',
          component: () => import('@/views/pages/Error'),
        },
      ],
    },
  ],
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
      if (store.getters.isLoggedIn) {
      next()
      return
    }
    next('/')
  } else {
    next()
  }
})

export default router
