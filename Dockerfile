FROM node:12.13.1-alpine3.10 as build-stage
LABEL maintainer="ohendriansyah@binar.co.id"
WORKDIR /usr/src/app
COPY package.json yarn.lock ./
RUN set -xe \
 && yarn install
COPY . .
RUN set -xe \
 && yarn run build

FROM nginx:1.17.6-alpine as production-stage
COPY --from=build-stage /usr/src/app/dist /usr/share/nginx/html
RUN set -xe \
 && sed -i 's/listen[[:space:]]*80;/listen 8080;/g' /etc/nginx/conf.d/default.conf
EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]
